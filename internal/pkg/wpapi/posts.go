package wpapi

import (
	"github.com/sourcegraph/conc/pool"
)

// AllPosts fetches all WordPress posts.
func AllPosts() (WPPosts, error) {
	const poolSize = 4

	totalPosts, err := totalPosts()
	if err != nil {
		return nil, err
	}

	totalPages := (totalPosts + maxPosts - 1) / maxPosts

	cache, err := NewPageCache()
	if err != nil {
		return nil, err
	}
	defer cache.Close()

	p := pool.NewWithResults[WPPosts]().
		WithMaxGoroutines(poolSize).
		WithErrors()

	for page := 1; page <= totalPages; page++ {
		page := page

		p.Go(func() (WPPosts, error) {
			posts, err := Posts(page, maxPosts, cache)
			if err != nil {
				return nil, err
			}
			return posts, nil
		})
	}

	res, err := p.Wait()
	if err != nil {
		return nil, err
	}

	data := make(WPPosts, 0, totalPosts)

	for _, s := range res {
		for _, p := range s {
			if p.IsValid() {
				data = append(data, p)
			}
		}
	}

	return data, nil
}
