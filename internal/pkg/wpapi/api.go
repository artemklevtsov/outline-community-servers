package wpapi

import (
	"os"

	"github.com/imroc/req/v3"
)

const (
	domain   = "outline.network"
	apiPath  = "/wp-json/wp/v2/posts"
	apiURL   = "https://" + domain + apiPath
	maxPosts = 100
)

func init() {
	if os.Getenv("DEBUG") == "true" {
		req.EnableDebugLog()
	}
}
