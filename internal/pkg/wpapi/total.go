package wpapi

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/imroc/req/v3"
)

func totalPosts() (int, error) {
	const key = "X-WP-Total"

	var apiErr WPError

	res, err := req.R().
		SetQueryParam("per_page", "1").
		SetErrorResult(&apiErr).
		Head(apiURL)

	if err != nil {
		return 0, fmt.Errorf("can't fetch number of posts: %w", err)
	}

	if res.IsErrorState() {
		return 0, fmt.Errorf("can't fetch number of posts: %w", apiErr)
	}

	value := res.Header.Get(key)
	if value == "" {
		return 0, fmt.Errorf("can't fetch number of posts: missing header %q", key)
	}

	total, err := strconv.Atoi(value)
	if err != nil {
		return 0, fmt.Errorf("can't fetch number of posts: invalid header %q", key)
	}

	if total == 0 {
		return 0, errors.New("no posts found")
	}

	return total, nil
}
