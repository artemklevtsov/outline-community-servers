package wpapi

import (
	"bytes"
	"encoding/gob"
	"errors"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/dgraph-io/badger/v3"
)

type PageCache struct {
	DB  *badger.DB
	Dir string
	TTL time.Duration
}

func NewPageCache() (*PageCache, error) {
	dbPath := filepath.Join(os.TempDir(), "cache-"+domain)
	dbOpts := badger.DefaultOptions(dbPath).
		WithLogger(nil)

	db, err := badger.Open(dbOpts)
	if err != nil {
		return nil, err
	}

	c := &PageCache{
		DB:  db,
		Dir: dbPath,
		TTL: 6 * time.Hour,
	}

	return c, nil
}

func (c *PageCache) Close() error {
	return c.DB.Close()
}

func (c *PageCache) Set(page int, posts *WPPosts) error {
	var (
		err  error
		buff bytes.Buffer
		enc  = gob.NewEncoder(&buff)
	)

	// Write data to cache
	err = c.DB.Update(func(txn *badger.Txn) error {
		buff.Reset()
		if err := enc.Encode(posts); err != nil {
			return err
		}
		entry := badger.NewEntry([]byte(strconv.Itoa(page)), buff.Bytes()).WithTTL(c.TTL)
		return txn.SetEntry(entry)
	})
	if err != nil {
		return err
	}

	return nil
}

func (c *PageCache) Get(page int) (WPPosts, error) {
	var (
		err   error
		posts WPPosts
		buff  bytes.Buffer
		dec   = gob.NewDecoder(&buff)
	)

	// Read data from cache
	err = c.DB.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(strconv.Itoa(page)))
		if err != nil {
			if errors.Is(err, badger.ErrKeyNotFound) {
				return nil
			} else {
				return err
			}
		}

		err = item.Value(func(val []byte) error {
			if _, err := buff.Write(val); err != nil {
				return err
			}
			return dec.Decode(&posts)
		})

		return err
	})

	if err != nil {
		return nil, err
	}

	return posts, nil
}
