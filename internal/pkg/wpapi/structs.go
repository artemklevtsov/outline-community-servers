package wpapi

// WPPosts is WPPost slice.
type WPPosts []WPPost

// WPPost is WordPress API post object.
type WPPost struct {
	Link      string     `json:"link"`
	Type      string     `json:"type"`
	Status    string     `json:"status"`
	Slug      string     `json:"slug"`
	Lang      string     `json:"lang"`
	CreatedAt CustomTime `json:"date_gmt"`
	UpdatedAt CustomTime `json:"modified_gmt"`
	Title     struct {
		Rendered string `json:"rendered"`
	} `json:"title"`
	Key struct {
		Type struct {
			Name string `json:"name"`
		} `json:"key_type"`
		Location struct {
			Name string `json:"name"`
		} `json:"key_location"`
		Hosting struct {
			Name string `json:"name"`
		} `json:"key_hosting"`
		Quality string `json:"key_quality"`
		SS      string `json:"key_ss"`
	} `json:"acf"`
}

// WPError is WordPress API error object.
type WPError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Data    struct {
		Status int `json:"status"`
	} `json:"data"`
}

func (e WPError) Error() string {
	return e.Message
}
