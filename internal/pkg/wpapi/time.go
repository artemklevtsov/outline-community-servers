package wpapi

import (
	"strings"
	"time"
)

// CustomTime time with custom parse format.
type CustomTime struct {
	time.Time
}

// UnmarshalJSON unmarshal JSON with custom time format.
func (ct *CustomTime) UnmarshalJSON(b []byte) error {
	const layout = "2006-01-02T15:04:05"

	s := strings.Trim(string(b), "\"")
	if s == "null" {
		ct.Time = time.Time{}
		return nil
	}

	t, err := time.Parse(layout, s)
	if err != nil {
		return err
	}

	ct.Time = t

	return nil
}
