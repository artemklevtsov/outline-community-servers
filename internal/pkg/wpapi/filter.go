package wpapi

import "strings"

// IsValid filters posts for the wpapi.
func (p *WPPost) IsValid() bool {
	return p.Type == "post" &&
		p.Status == "publish" &&
		p.Lang == "en" &&
		p.Key.Type.Name != "" &&
		strings.HasPrefix(p.Slug, "access-key")
}
