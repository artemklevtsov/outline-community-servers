package wpapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/corpix/uarand"
	"github.com/imroc/req/v3"
)

// Posts fetches WordPress API page with posts.
func Posts(page int, limit int, cache *PageCache) (WPPosts, error) {
	var (
		err    error
		posts  WPPosts
		apiErr WPError
	)

	posts, err = cache.Get(page)
	if err != nil {
		return nil, err
	}

	if len(posts) > 0 {
		return posts, nil
	}

	// WP API docs: https://developer.wordpress.org/rest-api/reference/posts/#list-posts
	res, err := req.R().
		SetHeader("user-agent", uarand.GetRandom()).
		SetQueryParam("page", strconv.Itoa(page)).
		SetQueryParam("per_page", strconv.Itoa(limit)).
		SetQueryParam("acf_format", "standard").
		SetSuccessResult(&posts).
		SetErrorResult(&apiErr).
		Get(apiURL)

	contentType := res.GetContentType()
	if !strings.Contains(contentType, "application/json") {
		return nil, fmt.Errorf("can't fetch page %d (status %s). Wrong content type: %q", page, res.Status, contentType)
	}

	var errJson *json.UnmarshalTypeError
	if err != nil && !errors.As(err, &errJson) {
		return nil, fmt.Errorf("can't fetch page %d (status %s). JSON parsing error: %w", page, res.Status, err)
	}

	if res.IsErrorState() {
		return nil, fmt.Errorf("can't fetch page %d (status %s). WordPress API error: %w", page, res.Status, apiErr)
	}

	err = cache.Set(page, &posts)
	if err != nil {
		return nil, err
	}

	return posts, nil
}
