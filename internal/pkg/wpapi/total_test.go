package wpapi

import (
	"net/http"
	"testing"

	"github.com/imroc/req/v3"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func Test_totalPosts(t *testing.T) {
	httpmock.ActivateNonDefault(req.GetClient())
	defer httpmock.DeactivateAndReset()

	t.Run("OK", func(t *testing.T) {
		httpmock.RegisterResponder("HEAD", apiURL, func(r *http.Request) (*http.Response, error) {
			resp := httpmock.NewBytesResponse(200, nil)
			resp.Header.Set("X-WP-Total", "11")
			return resp, nil
		})

		total, err := totalPosts()
		assert.Nil(t, err)
		assert.Equal(t, 11, total)
	})

	t.Run("Missing header", func(t *testing.T) {
		httpmock.RegisterResponder("HEAD", apiURL, func(r *http.Request) (*http.Response, error) {
			resp := httpmock.NewBytesResponse(200, nil)
			return resp, nil
		})

		total, err := totalPosts()
		assert.Error(t, err)
		assert.ErrorContainsf(t, err, "can't fetch number of posts", "missing header %q", "X-WP-Total")
		assert.Equal(t, 0, total)
	})
}
