package app

import (
	"cmp"
	"os"
	"slices"

	"github.com/pterm/pterm"
	"gitlab.com/artemklevtsov/outline-community-servers/internal/pkg/srvinfo"
)

type RunCmd struct {
	Output string `short:"o" env:"OUTPUT" help:"output file path" type:"path" default:"-"`
	Format string `short:"f" env:"FORMAT" help:"output format" enum:"json,yaml,csv,subscription,text,table" default:"table"`
	Status bool   `short:"s" help:"check servers status"`
	Alive  bool   `short:"a" help:"print only alive servers"`
}

type handlerFunc func(srvinfo.OutlineServers) ([]byte, error)

func (c *RunCmd) Run() error {
	handlers := map[string]handlerFunc{
		"json":         srvinfo.MarshalJSON,
		"yaml":         srvinfo.MarshalYAML,
		"csv":          srvinfo.MarshalCSV,
		"subscription": srvinfo.MarshalText,
		"text":         srvinfo.MarshalText,
		"table":        srvinfo.MarshalTable,
	}

	handler := handlers[c.Format]

	data, err := srvinfo.FetchData()
	if err != nil {
		return err
	}

	if c.Status || c.Alive {
		data.GetStatuses()
	}

	if c.Alive {
		tmp := make(srvinfo.OutlineServers, 0, len(data))
		// filter servers
		for i, s := range data {
			if s.Status == srvinfo.Up {
				tmp = append(tmp, data[i])
			}
		}

		data = tmp
	}

	slices.SortStableFunc(data, func(a, b *srvinfo.OutlineServer) int {
		return cmp.Compare(a.AccessKey, b.AccessKey)
	})

	// disable pterm style when save table to file
	if c.Output != "-" && c.Format == "table" {
		pterm.DisableStyling()
	}

	dec, err := handler(data)
	if err != nil {
		return err
	}

	var outFD *os.File
	if c.Output == "-" {
		outFD = os.Stdout
	} else {
		outFD, err = os.Create(c.Output)
		if err != nil {
			return err
		}

		defer outFD.Close()
	}

	_, err = outFD.Write(dec)
	if err != nil {
		return err
	}

	return nil
}
