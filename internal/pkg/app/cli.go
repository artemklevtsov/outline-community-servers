package app

import (
	"github.com/alecthomas/kong"
)

type CLI struct {
	Version kong.VersionFlag `short:"v" help:"Show version."`
	Run     RunCmd           `cmd:"" aliases:"r" help:"run application" default:"withargs"`
}
