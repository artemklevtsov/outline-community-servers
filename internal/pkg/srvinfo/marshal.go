package srvinfo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/jszwec/csvutil"
	"github.com/kyokomi/emoji/v2"
	"github.com/pterm/pterm"
	"gopkg.in/yaml.v3"
)

func MarshalJSON(data OutlineServers) ([]byte, error) {
	return json.MarshalIndent(data, "", "  ")
}

func MarshalYAML(data OutlineServers) ([]byte, error) {
	return yaml.Marshal(data)
}

func MarshalCSV(data OutlineServers) ([]byte, error) {
	return csvutil.Marshal(data)
}

func MarshalText(data OutlineServers) ([]byte, error) {
	var buf bytes.Buffer
	for _, i := range data {
		_, err := buf.Write([]byte(i.Link + "\n"))
		if err != nil {
			return nil, err
		}
	}

	return buf.Bytes(), nil
}

func MarshalTable(data OutlineServers) ([]byte, error) {
	tbl := make(pterm.TableData, 0, len(data)+1)

	// header
	row := []string{"Key", "Status", "Location", "Hosting", "Quality", "Link"}
	tbl = append(tbl, row)

	// rows
	for _, d := range data {
		row = []string{
			pterm.Cyan(strconv.Itoa(d.AccessKey)),
			colorStatus(d.Status),
			fmt.Sprintf("%s %s", flagCountry(d.Location), d.Location),
			d.Hosting,
			colorQuality(d.Quality),
			d.Link,
		}
		tbl = append(tbl, row)
	}

	res, err := pterm.DefaultTable.
		WithHasHeader().
		WithData(tbl).
		Srender()

	if err != nil {
		return nil, err
	}

	res += "\n"

	return []byte(res), nil
}

func colorStatus(s ServerStatus) string {
	switch s {
	case Up:
		return pterm.Green(s)
	case Down:
		return pterm.Red(s)
	case Unknown:
		return pterm.Gray(s)
	default:
		return string(s)
	}
}

func colorQuality(q int) string {
	s := strconv.Itoa(q)

	switch {
	case q >= 90:
		return pterm.Green(s)
	case q >= 70:
		return pterm.Cyan(s)
	case q >= 50:
		return pterm.Yellow(s)
	default:
		return pterm.Red(s)
	}
}

func flagCountry(c string) string {
	switch c {
	case "Hong Kong":
		return emoji.Sprint(":flag_Hong_Kong_SAR_China:")
	case "Bosnia and Herzegovina":
		return emoji.Sprint(":flag_Bosnia_&_Herzegovina:")
	default:
		return emoji.Sprintf(":flag_%s:", strings.ReplaceAll(c, " ", "_"))
	}
}
