package srvinfo

import (
	"net"
	"time"

	"github.com/sourcegraph/conc"
)

// ServerStatus status Outline server.
type ServerStatus string

const (
	Unknown ServerStatus = "Unknown"
	Up      ServerStatus = "Up"
	Down    ServerStatus = "Down"
)

const statusTimeout = 5 * time.Second

func scanHost(address string) ServerStatus {
	tcpConn, _ := net.DialTimeout("tcp", address, statusTimeout)
	if tcpConn != nil {
		tcpConn.Close()
		return Up
	}

	udpConn, _ := net.DialTimeout("udp", address, statusTimeout)
	if udpConn != nil {
		udpConn.Close()
		return Up
	}

	return Down
}

// GetStatus checks server online status.
func (s *OutlineServer) GetStatus() {
	s.Status = scanHost(s.Address())
}

// GetStatuses checks servers online status.
func (ss OutlineServers) GetStatuses() {
	// iter.ForEach(ss, func(val **OutlineServer) { (*val).GetStatus() })
	var wg conc.WaitGroup
	defer wg.Wait()

	for _, s := range ss {
		s := s

		wg.Go(func() {
			s.GetStatus()
		})
	}
}
