package srvinfo

import "errors"

var (
	ErrUrDecodRLInvalidPort        = errors.New("invalid port")
	ErrDecodeURLInvalidCredentials = errors.New("invalid credentials")
	ErrDecodeURLMissingPassword    = errors.New("missing password")
)
