package srvinfo

import "time"

// ServerInfo a shadowsocks server information.
type ServerInfo struct {
	Host     string       `json:"host" yaml:"host" csv:"host"`
	Port     int          `json:"port" yaml:"port" csv:"port"`
	Cipher   string       `json:"cipher" yaml:"cipher" csv:"cipher"`
	Password string       `json:"password" yaml:"password" csv:"password"`
	Status   ServerStatus `json:"-" yaml:"-" csv:"-"`
}

// OutlineServer is Outline server description.
type OutlineServer struct {
	AccessKey  int       `json:"access_key" yaml:"access_key" csv:"access_key"`
	Location   string    `json:"location" yaml:"location" csv:"location"`
	Hosting    string    `json:"hosting" yaml:"hosting" csv:"hosting"`
	Quality    int       `json:"quality" yaml:"quality" csv:"quality"`
	Published  time.Time `json:"published" yaml:"published" csv:"published"`
	KeyType    string    `json:"key_type" yaml:"key_type" csv:"key_type"`
	PageURL    string    `json:"page_url" yaml:"page_url" csv:"page_url"`
	Link       string    `json:"ss_link" yaml:"ss_link" csv:"ss_link"`
	ServerInfo `json:"server_info" yaml:"server_info"`
}

// OutlineServers is Outline servers list.
type OutlineServers []*OutlineServer
