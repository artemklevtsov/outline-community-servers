package srvinfo

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_decodeURL(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		url     string
		want    *ServerInfo
		wantErr error
	}{
		{
			name: "Access key 1002",
			url:  "ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpyNE1QNUR6RE5tQXFiQkh6YlNYdw@45.152.183.242:8388",
			want: &ServerInfo{
				Host:     "45.152.183.242",
				Port:     8388,
				Cipher:   "chacha20-ietf-poly1305",
				Password: "r4MP5DzDNmAqbBHzbSXw",
			},
			wantErr: nil,
		},
		{
			name: "Access key 1007",
			url:  "ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW9AMTU4LjU4LjE3My41NDo4MDk=",
			want: &ServerInfo{
				Host:     "158.58.173.54",
				Port:     809,
				Cipher:   "chacha20-ietf-poly1305",
				Password: "G!yBwPWH3Vao",
			},
			wantErr: nil,
		},
		{
			name: "Access key 1009",
			url:  "ss://YWVzLTI1Ni1nY206Rm9PaUdsa0FBOXlQRUdQQDE0Ni4wLjM2LjQ0OjczMDc=",
			want: &ServerInfo{
				Host:     "146.0.36.44",
				Port:     7307,
				Cipher:   "aes-256-gcm",
				Password: "FoOiGlkAA9yPEGP",
			},
			wantErr: nil,
		},
		{
			name: "Access key 1015",
			url:  "ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpLdzZoTVhRMlo3ZXk3bWYyVXF5cg@ak1015.www.getoutline.net:8388",
			want: &ServerInfo{
				Host:     "ak1015.www.getoutline.net",
				Port:     8388,
				Cipher:   "chacha20-ietf-poly1305",
				Password: "Kw6hMXQ2Z7ey7mf2Uqyr",
			},
			wantErr: nil,
		},
		{
			name: "Access key 1036",
			url:  "ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8=@ak1036.www.getoutline.net:805",
			want: &ServerInfo{
				Host:     "ak1036.www.getoutline.net",
				Port:     805,
				Cipher:   "chacha20-ietf-poly1305",
				Password: "G!yBwPWH3Vao",
			},
			wantErr: nil,
		},
		{
			name:    "Invalid credentials",
			url:     "ss//www.example.com:8090",
			want:    nil,
			wantErr: ErrDecodeURLInvalidCredentials,
		},
		{
			name:    "Invalid password",
			url:     "ss://dXNlcg==@www.example.com:8090",
			want:    nil,
			wantErr: ErrDecodeURLMissingPassword,
		},
		{
			name:    "Invalid port",
			url:     "ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8=@www.example.com",
			want:    nil,
			wantErr: ErrUrDecodRLInvalidPort,
		},
		{
			name:    "Missing encoded credentials",
			url:     "ss://d3d3LmV4YW1wbGUuY29tOjgwOQ==",
			want:    nil,
			wantErr: ErrDecodeURLInvalidCredentials,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, _ := url.Parse(tt.url)
			got, err := decodeURL(u)
			if tt.wantErr != nil {
				assert.Nil(t, got)
				assert.Error(t, err)
				assert.ErrorIs(t, err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got)
		})
	}
}
