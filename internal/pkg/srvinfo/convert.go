package srvinfo

import (
	"fmt"
	"net/url"
	"regexp"
	"strconv"

	"gitlab.com/artemklevtsov/outline-community-servers/internal/pkg/wpapi"
)

func parseInfo(post *wpapi.WPPost) (*OutlineServer, error) {
	quality, err := strconv.Atoi(post.Key.Quality)
	if err != nil {
		return nil, fmt.Errorf("can't parse server quality: %w", err)
	}

	var key int

	re := regexp.MustCompile(`(?i)Access\s+key\s+(\d+)`)
	matches := re.FindStringSubmatch(post.Title.Rendered)

	if len(matches) >= 2 {
		key, err = strconv.Atoi(matches[1])
		if err != nil {
			return nil, fmt.Errorf("can't parse Access Key: %w", err)
		}
	}

	ss, err := url.Parse(post.Key.SS)
	if err != nil {
		return nil, fmt.Errorf("can't parse ShadowSocks URL: %w", err)
	}

	// Remove URL path fragment
	ss.Fragment = ""

	info, err := decodeURL(ss)
	if err != nil {
		return nil, err
	}

	e := OutlineServer{
		AccessKey:  key,
		PageURL:    post.Link,
		Published:  post.CreatedAt.Time,
		Location:   post.Key.Location.Name,
		Hosting:    post.Key.Hosting.Name,
		Quality:    quality,
		KeyType:    post.Key.Type.Name,
		Link:       ss.String(),
		ServerInfo: *info,
	}

	return &e, nil
}
