package srvinfo

import (
	"gitlab.com/artemklevtsov/outline-community-servers/internal/pkg/wpapi"
)

// Run fetches Outline Community servers information.
func FetchData() (OutlineServers, error) {
	posts, err := wpapi.AllPosts()
	if err != nil {
		return nil, err
	}

	data := make(OutlineServers, len(posts))

	for i, post := range posts {
		data[i], err = parseInfo(&post)
		if err != nil {
			return nil, err
		}
	}

	return data, nil
}
