package srvinfo

import (
	"encoding/base64"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

// fixPad fix base64 encoding pad.
func fixPad(s string) string {
	if m := len(s) % 4; m != 0 {
		s += strings.Repeat("=", 4-m)
	}

	return s
}

// decodeURL decodes SS link.
func decodeURL(u *url.URL) (*ServerInfo, error) {
	var err error

	info := ServerInfo{}

	if u.User != nil {
		// Padding string if needed
		userEnc := fixPad(u.User.String())

		userDec, err := base64.StdEncoding.DecodeString(userEnc)
		if err != nil {
			return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", ErrDecodeURLInvalidCredentials)
		}

		splitted := strings.SplitN(string(userDec), ":", 2)
		if len(splitted) != 2 {
			return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", ErrDecodeURLMissingPassword)
		}

		info.Cipher = splitted[0]
		info.Password = splitted[1]
	} else {
		dec, err := base64.StdEncoding.DecodeString(u.Host)
		if err != nil {
			return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", err)
		}

		u, err = url.Parse("ss://" + string(dec))
		if err != nil {
			return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", err)
		}

		if u.User == nil {
			return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", ErrDecodeURLInvalidCredentials)
		}

		info.Cipher = u.User.Username()
		info.Password, _ = u.User.Password()
	}

	info.Host = u.Hostname()

	info.Port, err = strconv.Atoi(u.Port())
	if err != nil {
		return nil, fmt.Errorf("can't decode ShadowSocks URL: %w", ErrUrDecodRLInvalidPort)
	}

	return &info, nil
}
