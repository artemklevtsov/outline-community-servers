package srvinfo

import (
	"net"
	"strconv"
)

// Address returns network address.
func (s *OutlineServer) Address() string {
	return net.JoinHostPort(s.Host, strconv.Itoa(s.Port))
}
