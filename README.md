# outline-community-servers

[![GitLab CI Build Status](https://gitlab.com/artemklevtsov/outline-community-servers/badges/main/pipeline.svg)](https://gitlab.com/artemklevtsov/outline-community-servers/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/artemklevtsov/outline-community-servers)](https://goreportcard.com/report/gitlab.com/artemklevtsov/outline-community-servers)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Scrap Outline free access servers from the <https://outline.community/access-keys/>.

## Installation

```bash
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH
go install gitlab.com/artemklevtsov/outline-community-servers@latest
```

## Usage

```bash
❯ ./outline-community-servers-0.3.3 --help
Scrape the outline.community site and parse community servers credentials.

Usage:
  outline-community-servers-0.3.3 [flags]

Flags:
  -a, --alive           print only alive servers
  -f, --format string   output format (allowed "json", "yaml", "csv", "table", "subscriptionn") (default "table")
  -h, --help            help for outline-community-servers-0.3.3
  -o, --output string   output file path
  -s, --status          check servers status
  -v, --version         version for outline-community-servers-0.3.3
```

Example of results:

```bash
❯ ./outline-community-servers-0.3.3 -a
Key  | Status | Location             | ISP             | Quality | Link
1001 | Up     | Germany              | Contabo         | 94      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNToxejdrNUlNbFBzdms@167.86.82.191:443
1003 | Up     | France               | GANDI SAS       | 88      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpMRTZFZTZQRUxoVnU@92.243.24.236:443
1188 | Up     | Sweden               | Packet Exchange | 95      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1188.free.www.outline.network:809
1199 | Up     | Singapore            | M247            | 75      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1199.free.www.outline.network:803
1200 | Up     | United States        | GTHost          | 85      | ss://YWVzLTI1Ni1nY206ZzVNZUQ2RnQzQ1dsSklk@ak1200.free.www.outline.network:5004
1202 | Up     | Canada               | OVH SAS         | 88      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1202.free.www.outline.network:811
1203 | Up     | Germany              | myLoc           | 92      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1203.free.www.outline.network:804
1204 | Up     | United Arab Emirates | M247            | 91      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1204.free.www.outline.network:809
1209 | Up     | Italy                | Incubatec       | 90      | ss://YWVzLTI1Ni1nY206aGc0OSRXSDg5NDNnMw@ak1209.free.www.outline.network:18760
1210 | Up     | Japan                | M247            | 68      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1210.free.www.outline.network:809
1215 | Up     | Singapore            | M247            | 85      | ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpHIXlCd1BXSDNWYW8@ak1215.free.www.outline.network:803
1216 | Up     | Turkey               | Premier DC      | 65      | ss://YWVzLTI1Ni1nY206aGc0OSRXSDg5NDNnMw@ak1216.free.www.outline.network:18760
1219 | Up     | Germany              | GTHost          | 93      | ss://YWVzLTI1Ni1nY206ZTRGQ1dyZ3BramkzUVk@ak1219.free.www.outline.network:9102
```
