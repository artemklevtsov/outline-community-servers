MODULE   := $(shell go list -m)
MODNAME  := $(shell go list -m | xargs basename)
VERSION  := $(shell git tag --sort=-version:refname | head -n 1 | sed 's/^v//')
OUTFILE  := $(MODNAME)
LDFLAGS  := -w -s -X main.version=$(VERSION)
SOURCES  := $(wildcard *.go)

all: test build data

release:
	goreleaser release --skip-publish

data: csv json yaml subscription online

clean:
	go clean
	rm -f $(OUTFILE)
	rm -f dist/*

build: $(SOURCES)
	go build -ldflags "$(LDFLAGS)" -o "$(OUTFILE)" $(SOURCES)

test:
	go test ./...

coverage:
	go test ./... -coverprofile=coverage.out

dep:
	go mod download

vet:
	go vet

lint:
	golangci-lint run

version:
	@echo $(VERSION)

modname:
	@echo $(MODNAME)

$(OUTFILE): build

run: $(OUTFILE)
	./$(OUTFILE) -a -f table

csv: $(OUTFILE)
	./$(OUTFILE) -f csv -o data/data.csv

json: $(OUTFILE)
	./$(OUTFILE) -f json -o data/data.json

yaml: $(OUTFILE)
	./$(OUTFILE) -f yaml -o data/data.yaml

subscription: $(OUTFILE)
	./$(OUTFILE) -a -f subscription -o data/subscription

online: $(OUTFILE)
	./$(OUTFILE) -a -f table -o data/online
