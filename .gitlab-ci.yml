variables:
  CGO_ENABLED: 0
  GOCACHE: ${CI_PROJECT_DIR}/.cache/go
  BINFILE: outline-community-servers
  GOLANG_VERSION: "1.21.0"
  GOLANGCI_VERSION: "1.54.0"
  GORELEASER_VERSION: "1.20.0"

stages:
  - build
  - test
  - deploy

build:
  stage: build
  image: golang:${GOLANG_VERSION}-alpine
  before_script:
    - apk add make git
  script:
    - make build
  artifacts:
    paths:
      - ${BINFILE}
    expire_in: 3 days
  cache:
    paths:
      - ${GOCACHE}

lint:
  stage: test
  image: golangci/golangci-lint:v${GOLANGCI_VERSION}-alpine
  script:
    - golangci-lint run

test:
  stage: test
  image: golang:${GOLANG_VERSION}-alpine
  before_script:
    - apk add make git
  script:
    - make test
  cache:
    paths:
      - ${GOCACHE}

release:
  stage: deploy
  image:
    name: goreleaser/goreleaser:v${GORELEASER_VERSION}
    entrypoint: ['']
  only:
    - tags
  variables:
    # Disable shallow cloning so that goreleaser can diff between tags to
    # generate a changelog.
    GIT_DEPTH: 0
  script:
    - goreleaser release --clean
  cache:
    paths:
      - ${GOCACHE}

data:
  stage: deploy
  only:
    - schedules
  image: golang:${GOLANG_VERSION}-alpine
  variables:
    BOT_ENMAIL: "bot@gitlab.com"
    BOT_NAME: "GitLab Robot"
    BRANCH: main
  allow_failure: true
  retry: 2
  before_script:
    - apk add make git
  script:
    - make data
  after_script:
    - cd ${CI_PROJECT_DIR}
    - lines=$(git diff -U0 data | grep '^+[^+]' | wc -l)
    - if [ ${lines} -eq 0 ]; then exit 0; fi;
    - git config --global user.name "${BOT_ENMAIL}"
    - git config --global user.email "${BOT_NAME}"
    - git add data/*
    - git commit -m "Update data at $(date +%Y-%m-%d)"
    - git push -o ci.skip "https://CiBot:${CI_PUSH_TOKEN}@${CI_REPOSITORY_URL#*@}" HEAD:${BRANCH}
  cache:
    paths:
      - ${GOCACHE}
