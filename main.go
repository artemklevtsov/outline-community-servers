package main

import (
	"github.com/alecthomas/kong"
	"gitlab.com/artemklevtsov/outline-community-servers/internal/pkg/app"
)

var version = "devel"

func main() {
	cli := app.CLI{}

	ctx := kong.Parse(
		&cli,
		kong.Description("Fetch Outline Community Servers credentials"),
		kong.ConfigureHelp(kong.HelpOptions{
			Summary: true,
			Compact: true,
			Tree:    true,
		}),
		kong.Vars{
			"version": version,
		},
	)

	err := ctx.Run()
	ctx.FatalIfErrorf(err)
}
